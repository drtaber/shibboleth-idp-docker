# Shibboleth-idp-docker

This is a very basic starter to get a shibboleth-idp container up and running for your environment. It creates a very basic Docker image, and utilizes environmental variables to pull your configuration and customization files from a git repository. It is recommended that you use a private git repository that requires a password for file management due to the sensitivity of some of your files. If you don't have access to a private git repository, one can be requested from the UNC System Office by submitting a request to system-requests@northcarolina.edu.

## Overview
This document and the associated files are meant to assist you with getting a Shibboleth IDP image compiled and a container started. It does not perform any customization. 

### Prerequisites 

#### Docker

Running Docker containers requires the Docker container engine. For high availability features, you can look into applications like kubernetes or the VMware PKS product. Both are behind the scope of this document.
You can install Docker on RHEL with the following command.: 

`yum install docker`

Note: Use the Red Hat supplied Docker package. It includes custom Red Hat hooks to deal with licensing issues with Satellite products.

Docker status check `systemctl status docker`

Start docker: `systemctl start docker`



#### Git

For purposes of building this Dockerfile, it is recommended you clone this git repo locally. It will put all of the files where they need to be for building the container.

### Cloning the Docker Repo

To start the process, clone this repository to your local machine with the following command: 

`git clone https://gitlab.northcarolina.edu/drtaber/shibboleth-idp-docker.git`

### Building the Docker Image

Building the Docker image to run your containers is relatively straight forward. In cases of PROXY requirements, you may need to make some adjustments, but otherwise it should run out of the box and make a relatively up-to-date Docker Image for you. 
Note: We're moving in the direction of creating a Image repo for our campuses to expedite this process, but until that happens, I will update this Dockerfile monthly or so.

You can build the image with the following command in the directory where you cloned the git repo above. :

`docker build -t shibboleth-idp .`

If you want to version your image files, you can add version tags to the end of the image with version and/or date: 

`docker build -t shibboleth-idp:3.4.4.20190529 .`
*Note: Make sure you are in the shibboleth-idp-docker directory before running the above command*

### Running a container

Once an image has been compiled, you will need to customize the start_idp.sh script to reflect your personal git repo information. It uses these environment variables to customize each container within your environment to your specifications. It also allows you to port your configuration between host servers, IDP versions, and manage version/changes. Everything in {} brackets is a required setting. The descriptions are below with examples:

* {IP_ADDRESS} : The IP address that will be assigned and mapped to the container
* {GIT_USER} : The Username for your GIT repository for HTTPS access
* {GIT_PASSWORD} : The Password for your GIT repository for HTTPS Access
* {GITREPO} : The repository where your configuration is stored - without the 'http:// or https://'
* {SHIB_PASSWORD} : The Password for your shibboleth certificates and keys
* {VHOST} : The virtual repo project name that you used in git. 
* {CONTAINER NAME} : The name of the Container you wish to identify the container with in docker. This is not the same as the hostname and is only visible outside of the container on the Docker host
* {IMAGE NAME}: The image you want to start the container from. This should match the name you used in the previous step, Building the Docker Image

An example file might look like: 

`docker run -d -p 1.2.3.3:443:4443 -p 1.2.3.3:8443:8443 -e GIT_USER=mygituser -e GIT_PASS='SecretPassword!' -e GITREPO='gitlab.northcarolina.edu/mygituser/devidp.northcarolina.edu.git' -e JETTY_BROWSER_SSL_KEYSTORE_PASSWORD='shibpass' -e JETTY_BACKCHANNEL_SSL_KEYSTORE_PASSWORD='shibpass' -e JETTY_MAX_HEAP=2048m -e VHOST=devidp.northcarolina.edu --name=devidp.northcarolina.edu shibboleth-idp:3.4.4.20190529`

When you execute that script, it will start your container with a default skeleton installation of the shibboleth IDP. At this point the IDP *should start properly*.It is however, in a very basic state. From here you will need to start customizing files and committing them to git to make them accessible to the container.


### Helpful commands

**Docker**
* List docker images: `docker image ls`
* Delete an image: `docker rmi imagename` (or imageid)
* Delete a bunch of images: `docker rmi imageid imageid imageid (OR docker images | grep 'none' | awk -F ' ' '{print $3}' | xargs docker rmi )`
* List containers: `docker ps -a`
* Stop a container: `docker stop containername`
* Delete a container: `docker rm containername`
* Step into the container: `docker exec -t -i containername /bin/bash`
* Get container log info: `docker logs containername`
* Get Jetty Log info: `docker exec -t -i containername bash more /opt/jetty-home/logs/{log-name}.log`

**Shibboleth IdP**
* Sync the shibboleth install from git: `docker exec -t -i containername bash -c 'cd /bin;sh sync-git.sh'`
* Update your shibboleth config files etc from git: `docker exec -t -i containername bash -c 'cd /opt;git pull'`
* Create a shibboleth keypair (to become idp-encryption, idp-signing, and idp-backchannel): `/usr/bin/openssl req -new -x509 -nodes -newkey rsa:2048 -text -keyout {{ new_key }}.key -days 3650 -sha256 -subj '/CN={{idp_name}}' -out {{ new_cert }}.crt`
* Get Shib logs: `docker exec -t -i containername bash more /opt/shibboleth-idp/logs/idp-process.log`


### IDP Configuration

**Files to Edit**

Your IDP will likely need edits to the following files (and then have them uploaded to Git)
* conf/attribute-filter.xml
* conf/attribute-resolver.xml
* conf/idp.properties
* conf/ldap.properties
* conf/logback.xml
* conf/metadata-providers.xml
* conf/relying-party.xml
* conf/saml-nameid.xml
* conf/metadata/unc_federation.pem
* credentials/idp-backchannel.crt
* credentials/idp-browser.p12
* credentials/idp-encryption.crt
* credentials/idp-encryption.key
* credentials/idp-signing.crt
* credentials/idp-signing.key
* views/login.vm
* messages/messages.properties - Note: New consolidated messages.properties file
In version 3.4.4, messages.properties is the only properties file from messages/ loaded in services.xml. When upgrading from older versions with split properties files, you will need to copy over the properties like idp.logo, and others to this new file and rebuild war file. 


### Helpful commands to execute after making IDP configuration changes

*  Sync the shibboleth install from git: `docker exec -t -i containername bash -c 'cd /bin;sh sync-git.sh'`
*  Update your shibboleth install from git: `docker exec -t -i containername bash -c 'cd /opt;git pull'`
*  Rebuild war file: `docker exec -t -i containername bash -c 'cd /opt/shibboleth-idp/bin;sh build.sh build-war'`
*  Restart IDP container: `docker restart  containername`


### Common Errors and Issues
*  **Unable to find repo locally** - Check the GITREPO path and make sure it does not include http:// or https://
*  **ports not showing in docker ps for container** - IP was not assigned. Check the IPAddress in the start_*. sh script
*  **Error: Peer’s certificate issuer is not recognized** - run this command: `git config --global http.sslVerify "false"`
*  **Jetty Error: Error loading key named 'secret1'** - Jetty keystore password issue. Make sure the Dockerfile password matches what's in the idp.properties


### Commands cheat sheet
In an editor, search and replace *containername*  with your container’s name for a quick reference

`docker stop containername`

`docker start containername`

`docker rm containername`

`docker logs containername`

`docker exec -t -i containername /bin/bash`

`docker exec -t -i containername bash -c 'cd /bin;sh sync-git.sh'`

`docker exec -t -i containername bash -c 'cd /opt;git pull'`

`docker exec -t -i containername bash -c 'cd /opt/shibboleth-idp/bin;sh build.sh build-war'`

`docker exec -t -i containername bash -c 'cd /bin;sh run-jetty.sh'`

`docker restart containername`

`docker exec -t -i containername  bash -c 'tail -f /opt/shibboleth-idp/logs/idp-process.log'`

`docker exec -t -i containername bash -c 'tail -f  /opt/jetty-home/logs/YYYY_MM_DD.jetty.log'`

