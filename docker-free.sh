#!/bin/bash

#IMPORTANT README INFO
# This script will create a basic IDP installation on the local operating system
# It will not install and build a docker image or container
# It will not give you an easy upgrade path
# It will not give you the ability to easily nuke and start over
# It will install openjdk, jetty, and shibboleth IDP
# You will be responsible for all updates and configuration of jetty and the IDP software. There will be minimal or no support from the UNC System Office.

export JETTY_HOME=/opt/jetty-home
export JAVA_HOME=/usr/lib/jvm/jre 
export JETTY_MAX_HEAP=2048m 
export PATH=$PATH:$JRE_HOME/bin
export IDP_VERSION=3.4.4
export jetty_version=9.4.27.v20200227
# Install Necessary YUM Packages
yum -y install deltarpm yum-utils wget tar which java-11-openjdk git 

# Create Jetty User
useradd jetty -U -s /bin/false \

# Download Jetty, and install, initialize a new base 
cd /opt
wget -O jetty-distribution-$jetty_version.tar.gz "https://repo1.maven.org/maven2/org/eclipse/jetty/jetty-distribution/$jetty_version/jetty-distribution-$jetty_version.tar.gz" 
tar -zxvf jetty-distribution-$jetty_version.tar.gz -C /opt 
rm jetty-distribution-$jetty_version.tar.gz 
ln -s /opt/jetty-distribution-$jetty_version/ /opt/jetty-home 

# Config Jetty init script (old style)
cp /opt/jetty-home/bin/jetty.sh /etc/init.d/jetty 

# Download Shibboleth IdP and install 
wget https://shibboleth.net/downloads/identity-provider/$IDP_VERSION/shibboleth-identity-provider-$IDP_VERSION.tar.gz 
tar -zxvf  shibboleth-identity-provider-$IDP_VERSION.tar.gz -C /opt 
rm /shibboleth-identity-provider-$IDP_VERSION.tar.gz \


#Download Logback
mkdir -p $JETTY_HOME/lib/logging 
wget http://logback.qos.ch/dist/logback-1.1.7.tar.gz 
tar -zxvf logback-1.1.7.tar.gz -C /opt 
mv /opt/logback-1.1.7/*.jar $JETTY_HOME/lib/logging/ 

#Download slf4j
wget http://www.slf4j.org/dist/slf4j-1.7.21.tar.gz 
tar -zxvf  slf4j-1.7.21.tar.gz -C /opt 
mv /opt/slf4j-1.7.21/slf4j-api-1.7.21.jar $JETTY_HOME/lib/logging/ 

# Download the library to allow SOAP Endpoints, and place 
wget https://build.shibboleth.net/nexus/content/repositories/releases/net/shibboleth/utilities/jetty9/jetty9-dta-ssl/1.0.0/jetty9-dta-ssl-1.0.0.jar 
mv jetty9-dta-ssl-1.0.0.jar /opt/jetty-home/lib/ext/ 

# Setting owner ownership and permissions on new items in this command
chown -R root:jetty /opt 
chmod -R 640 /opt 

chmod +x /opt/shibboleth-identity-provider-${IDP_VERSION}/bin/*.sh 
/opt/shibboleth-identity-provider-${IDP_VERSION}/bin/install.sh 



# Setting owner ownership and permissions on new items from the COPY command
mkdir $JETTY_HOME/tmp 
chmod 750 /opt/shibboleth-idp/bin/* 
chown -R root:jetty /opt 
chmod -R 750 /opt/shibboleth-idp/bin 

