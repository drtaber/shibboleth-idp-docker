git config --global http.sslverify false
git clone https://$GIT_USER:$GIT_PASS@$GITREPO /tmp/$VHOST
cp -rf /tmp/$VHOST/shibboleth-idp/* /opt/shibboleth-idp/
cp -rf /tmp/$VHOST/jetty-home/* /opt/jetty-home/
cp -r /tmp/$VHOST/.git /opt/.git