FROM rhel7.8

MAINTAINER drtaber@northcarolina.edu

LABEL idp.java.version="11.0.1.13-3" \
      idp.jetty.version="9.4.30.v20200611" \
      idp.version="3.4.7"

ENV JETTY_HOME=/opt/jetty-home \
    JAVA_HOME=/usr/lib/jvm/jre \
    JETTY_MAX_HEAP=2048m \
    PATH=$PATH:$JRE_HOME/bin\
    TERM=xterm \
    IDP_VERSION=3.4.7 \
    slf4j_version=1.7.9 \
    logback_version=1.2.3
    
# Fix per https://bugzilla.redhat.com/show_bug.cgi?id=1192200
RUN yum -y install deltarpm yum-utils wget tar which java-11-openjdk git --disablerepo=*-eus-* --disablerepo=*-htb-* \
    --disablerepo=*-ha-* --disablerepo=*-rt-* --disablerepo=*-lb-* --disablerepo=*-rs-* --disablerepo=*-sap-*


RUN yum-config-manager --disable *-eus-* *-htb-* *-ha-* *-rt-* *-lb-* *-rs-* *-sap-* > /dev/null
RUN yum-config-manager --enable rhel-7-server-optional-rpms > /dev/null

RUN set -x; \
    java_version=11.0.1.13-3; \
    jetty_version=9.4.30.v20200611; \
    idp_version=3.4.7; \
    useradd jetty -U -s /bin/false \

# Download Jetty, verify the hash, and install, initialize a new base \
    && cd / \
    && wget -O jetty-distribution-$jetty_version.tar.gz "https://repo1.maven.org/maven2/org/eclipse/jetty/jetty-distribution/$jetty_version/jetty-distribution-$jetty_version.tar.gz" \
    && tar -zxvf jetty-distribution-$jetty_version.tar.gz -C /opt \
    && rm jetty-distribution-$jetty_version.tar.gz \
    && ln -s /opt/jetty-distribution-$jetty_version/ /opt/jetty-home \

# Config Jetty \
    && cd / \
    && cp /opt/jetty-home/bin/jetty.sh /etc/init.d/jetty \

# Download Shibboleth IdP, verify the hash, and install \
    && cd / \ 
    && wget https://shibboleth.net/downloads/identity-provider/archive/$idp_version/shibboleth-identity-provider-$idp_version.tar.gz \
    && tar -zxvf  shibboleth-identity-provider-$idp_version.tar.gz -C /opt \
    && rm /shibboleth-identity-provider-$idp_version.tar.gz \
#    && ln -s /opt/shibboleth-identity-provider-$idp_version/ /opt/shibboleth-idp \

#Download Logback & slf4j
    && mkdir -p $JETTY_HOME/lib/logging \
    && cd $JETTY_HOME/lib/logging \
    && wget -q https://repo1.maven.org/maven2/org/slf4j/slf4j-api/$slf4j_version/slf4j-api-$slf4j_version.jar \
    && wget -q https://repo1.maven.org/maven2/ch/qos/logback/logback-classic/$logback_version/logback-classic-$logback_version.jar \
    && wget -q https://repo1.maven.org/maven2/ch/qos/logback/logback-core/$logback_version/logback-core-$logback_version.jar \
    && wget -q https://repo1.maven.org/maven2/ch/qos/logback/logback-access/$logback_version/logback-access-$logback_version.jar \

# Download the library to allow SOAP Endpoints, verify the hash, and place \
    && cd / \
    && wget https://build.shibboleth.net/nexus/content/repositories/releases/net/shibboleth/utilities/jetty9/jetty9-dta-ssl/1.0.0/jetty9-dta-ssl-1.0.0.jar \
    && mv jetty9-dta-ssl-1.0.0.jar /opt/jetty-home/lib/ext/ \


# Setting owner ownership and permissions on new items in this command
    && chown -R root:jetty /opt \
    && chmod -R 640 /opt 

RUN chmod +x /opt/shibboleth-identity-provider-${IDP_VERSION}/bin/*.sh \
    && /opt/shibboleth-identity-provider-${IDP_VERSION}/bin/install.sh -Didp.host.name=${HOSTNAME} -Didp.src.dir=/opt/shibboleth-identity-provider-${IDP_VERSION} -Didp.target.dir=/opt/shibboleth-idp -Didp.entityID=https://${HOSTNAME}/idp -Didp.scope=${HOSTNAME} -Didp.keystore.password=000 -Didp.sealer.password=555 

COPY bin/ /usr/local/bin/

# Setting owner ownership and permissions on new items from the COPY command
RUN mkdir $JETTY_HOME/tmp \
    && chmod 750 /opt/shibboleth-idp/bin/* \
    && chown -R root:jetty /opt \
    && chmod -R 750 /opt/shibboleth-idp/bin \
    && chmod 750 /usr/local/bin/run-jetty.sh \
    && chmod 750 /usr/local/bin/sync-git.sh

# Opening 4443 (browser TLS), 8443 (mutual auth TLS)
EXPOSE 4443 8443

CMD ["run-jetty.sh"]